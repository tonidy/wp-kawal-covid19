# kawalcovid19-wp

WordPress backend for [kawalcovid19.id](https://kawalcovid19.id). Credit to [mhoofman/wordpress-heroku](https://github.com/mhoofman/wordpress-heroku).

The deployed version can be accessed via `https://kawalcovid19-wp.herokuapp.com/wp/wp-json/`

## Installation

With the [Heroku CLI](http://devcenter.heroku.com/articles/heroku-command), create your app

    $ cd wordpress-heroku
    $ heroku create
    Creating app... done, ⬢ obscure-fjord-98993
    https://obscure-fjord-98993.herokuapp.com/ | https://git.heroku.com/obscure-fjord-98993.git

Add a database to your app

    $ heroku addons:create cleardb
    Creating cleardb on ⬢ obscure-fjord-98993... free
    Created cleardb-spherical-32517 as CLEARDB_COPPER_URL
    Use heroku addons:docs cleardb to view documentation

## Required Envs

Please add the following envs into your Heroku config vars:

- `AUTH_KEY`
- `AUTH_SALT`
- `AWS_ACCESS_KEY_ID`
- `AWS_SECRET_ACCESS_KEY`
- `CLEARDB_DATABASE_URL`
- `LOGGED_IN_KEY`
- `LOGGED_IN_SALT`
- `NONCE_KEY`
- `NONCE_SALT`
- `SECURE_AUTH_KEY`
- `SECURE_AUTH_SALT`

## Deploy

Deploy to Heroku

    $ git push heroku master

## Usage

The file system associated with a Heroku app is ephemeral.  Add new
plugins and themes to the local repository and then push to Heroku.

